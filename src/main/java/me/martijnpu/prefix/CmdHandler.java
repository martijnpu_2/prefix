package me.martijnpu.prefix;

import me.martijnpu.prefix.Util.*;
import me.martijnpu.prefix.Util.Config.ConfigData;
import me.martijnpu.prefix.Util.Tags.Tag;
import me.martijnpu.prefix.Util.Tags.TagManager;

import static me.martijnpu.prefix.Core.hasAnColorPermission;
import static me.martijnpu.prefix.Core.hasColorPermission;
import static me.martijnpu.prefix.Util.Statics.*;

public class CmdHandler {
    private boolean isPrefix;
    private Object sender;
    private boolean isPlayer;
    private String[] args;
    private Object player;
    private Color color;

    public void onPrefixCommand(Object sender, boolean isPlayer, String[] args) {
        if (!isPlayer)
            sender = null;
        this.isPrefix = true;
        this.sender = sender;
        this.isPlayer = isPlayer;
        this.args = args;
        this.player = sender;

        if (args.length == 0) {
            Messages.CMD_UNKNOWN.send(sender, "prefix");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "help":
                onHelpCmd();
                break;

            case "color":
                onColorCmd();
                break;

            case "name":
                onNameCmd();
                break;

            case "bracket":
                onBracketCmd();
                break;

            case "list":
                ListViewer.showPrefixColorList(sender);
                break;

            case "reset":
                onResetCmd();
                break;

            case "reload":
                onReloadCmd();
                break;

            case "version":
                onVersionCmd();
                break;

            case "debug":
                if (onDebugCmd())
                    break;

            default: {
                onDefaultCmd();
                break;
            }
        }
    }

    public void onSuffixCommand(Object sender, boolean isPlayer, String[] args) {
        if (!isPlayer)
            sender = null;
        this.isPrefix = false;
        this.sender = sender;
        this.isPlayer = isPlayer;
        this.args = args;
        this.player = sender;

        if (args.length == 0) {
            Messages.CMD_UNKNOWN.send(sender, "suffix");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "help":
                onHelpCmd();
                break;

            case "color":
                onColorCmd();
                break;

            case "list":
                ListViewer.showSuffixColorList(sender);
                break;

            case "reset":
                onResetCmd();
                break;

            case "remove":
                onRemoveCmd();
                break;

            case "debug":
                if (onDebugCmd())
                    break;

            default:
                onDefaultCmd();
                break;
        }
    }

    /**
     * Suffix & Prefix
     */
    private void onHelpCmd() {
        if (Permission.TARGET_OTHER.hasPermission(sender))
            Messages.HELP_OTHERS.sendBig(sender);
        else
            Messages.HELP.sendBig(sender);
    }

    /**
     * Suffix & Prefix
     */
    private void onColorCmd() {
        if (isPlayer && !hasAnColorPermission(sender, isPrefix ? Permission.COLOR_PREFIX : Permission.COLOR_SUFFIX)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length <= 1) {
            if (Permission.TARGET_OTHER.hasPermission(sender))
                Messages.CMD_USAGE.send(sender, (isPrefix ? "/prefix" : "/suffix") + " color <color> [name]");
            else
                Messages.CMD_USAGE.send(sender, (isPrefix ? "/prefix" : "/suffix") + " color <color>");
            return;
        }

        if (args.length >= 3) {
            player = getPlayer(sender, args[2]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.send((isPrefix ? "/prefix" : "/suffix") + " color <color> <name>");
            return;
        }

        if (args[1].equalsIgnoreCase("reset")) {
            if (isPrefix) {
                if (Core.resetPrefixColor(sender, player) && sender != player)
                    Messages.CMD_RESET_OTHER.send(player, "prefix", getDisplayName(player));
            } else {
                if (Core.resetSuffixColor(sender, player) && sender != player)
                    Messages.CMD_RESET_OTHER.send(player, "suffix", getDisplayName(player));
            }
            return;
        }

        color = Color.checkValidColor(sender, args[1]);
        if (color == null)
            return;

        if (color == Color.HEXADECIMAL) {
            String hex = HexColor.getAsHex(args[1]);
            if (hex != null)
                args[1] = hex;
        }

        if (!hasColorPermission(sender, color, isPrefix ? Permission.COLOR_PREFIX : Permission.COLOR_SUFFIX)) {
            Messages.NO_PERM_COLOR.send(sender);
            return;
        }

        if (isPrefix) {
            if (Core.changePrefixColor(sender, player, color, args[1]) && sender != player)
                Messages.COLOR_OTHER.send(sender, "prefix", getDisplayName(player));
        } else {
            if (Core.changeSuffixColor(sender, player, color, args[1]) && sender != player)
                Messages.COLOR_OTHER.send(sender, "suffix", getDisplayName(player));
        }
    }

    /**
     * Prefix Only
     */
    private void onNameCmd() {
        if (!isPrefix)
            return;

        if (isPlayer && !hasAnColorPermission(sender, Permission.COLOR_NAME)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length == 1) {
            if (Permission.TARGET_OTHER.hasPermission(sender))
                Messages.CMD_USAGE.send(sender, "/prefix name <color> [name]");
            else
                Messages.CMD_USAGE.send(sender, "/prefix name <color>");
            return;
        }

        if (args.length >= 3) {
            player = getPlayer(sender, args[2]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole("/prefix name <color> <name>");
            return;
        }

        if (args[1].equalsIgnoreCase("reset")) {
            if (Core.resetNameColor(sender, player) && sender != player)
                Messages.CMD_RESET_OTHER.send(sender, "name", getDisplayName(player));
            return;
        }

        color = Color.checkValidColor(sender, args[1]);
        if (color == null)
            return;

        if (color == Color.HEXADECIMAL && ConfigData.HEX_NAMES.get()) {
            String hex = HexColor.getAsHex(args[1]);
            if (hex != null)
                args[1] = hex;
        }

        if (!hasColorPermission(sender, color, Permission.COLOR_NAME)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (Core.changeNameColor(sender, player, color, args[1]) && sender != player)
            Messages.COLOR_OTHER.send(sender, "name", getDisplayName(player));
    }

    /**
     * Prefix Only
     */
    private void onBracketCmd() {
        if (!isPrefix)
            return;

        if (!ConfigData.PREFIX_BRACKET_COLOR.get().isEmpty()) {
            Messages.ERROR_BRACKET.send(sender);
            return;
        }

        if (isPlayer && !hasAnColorPermission(sender, Permission.COLOR_BRACKET)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length == 1) {
            if (Permission.TARGET_OTHER.hasPermission(sender))
                Messages.CMD_USAGE.send(sender, "/prefix bracket <color> [name]");
            else
                Messages.CMD_USAGE.send(sender, "/prefix bracket <color>");
            return;
        }

        if (args.length >= 3) {
            player = getPlayer(sender, args[2]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole("/prefix bracket <color> <name>");
            return;
        }

        if (args[1].equalsIgnoreCase("reset")) {
            if (Core.resetBracketColor(sender, player) && sender != player)
                Messages.CMD_RESET_OTHER.send(sender, "bracket", getDisplayName(player));
            return;
        }

        color = Color.checkValidColor(sender, args[1]);
        if (color == null)
            return;

        if (color == Color.HEXADECIMAL && ConfigData.HEX_NAMES.get()) {
            String hex = HexColor.getAsHex(args[1]);
            if (hex != null)
                args[1] = hex;
        }

        if (!hasColorPermission(sender, color, Permission.COLOR_BRACKET)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (Core.changeBracketColor(sender, player, color, args[1]) && sender != player)
            Messages.CMD_CHANGED_OTHER.send(sender, "bracket", getDisplayName(player));
    }

    /**
     * Suffix & Prefix
     */
    private void onResetCmd() {
        if (!(isPrefix ? Permission.PREFIX_CHANGE : Permission.SUFFIX_CHANGE).hasPermission(sender)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length >= 2) {
            player = getPlayer(player, args[1]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole((isPrefix ? "/prefix" : "/suffix") + " reset <name>");
            return;
        }

        if (isPrefix) {
            Core.resetPrefix(player);
            if (player != sender)
                Messages.CMD_RESET_OTHER.send(sender, "prefix", getDisplayName(player));
        } else {
            Core.resetSuffix(player);
            if (player != sender)
                Messages.CMD_RESET_OTHER.send(sender, "suffix", getDisplayName(player));
        }
    }

    /**
     * Independent
     */
    private void onReloadCmd() {
        if (!Permission.ADMIN.hasPermission(sender)) {
            Messages.NO_PERM_CMD.send(sender);
        } else {
            ConfigData.getInstance().reload(sender);
        }
    }

    /**
     * Independent
     */
    private void onVersionCmd() {
        if (!Permission.ADMIN.hasPermission(sender)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        Messages.PLUGIN.sendBig(sender, ""
                + "&fAuthor            :&a martijnpu"
                + "\n&fBungeecord     :&a " + (isProxy ? "Yes" : "No")
                + "\n&fServer           :&a " + Statics.getServerVersion()
                + "\n&fLocale           :&a " + ConfigData.LOCALE.get()
                + "\n&fYour Version    :&a " + currVersion + (newVersion > currVersion ? " &c(outdated)" : "") + (newVersion < currVersion ? " &3(Dev Version)" : "")
                + "\n&fNewest version :&a " + (newVersion == 0 ? "N/A" : newVersion)
                + "\n&fWebsite           :&a www.spigotmc.org/resources/prefix.70359"
                + "\n&fSupport          :&a https://discord.gg/2RHYvNy"
                + (debug ? "\n&cDEBUG MODE      &f:&4 ENABLED" : ""));
    }

    /**
     * Suffix & Prefix
     *
     * @return Debug enabled (Don't skip command)
     */
    private boolean onDebugCmd() {
        if (Statics.debug) {
            if (!Permission.ADMIN.hasPermission(sender)) {
                Messages.NO_PERM_CMD.send(sender);
                return true;
            }

            if (args.length >= 2) {
                player = getPlayer(sender, args[1]);
                if (player == null)
                    return false; //Already handled
            } else if (!isPlayer) {
                Messages.ERROR_TARGET_NEED.sendConsole();
                Messages.CMD_USAGE.sendConsole((isPrefix ? "/prefix" : "/suffix") + " debug <name>");
                return false;
            }

            Messages.DEBUG.sendConsole("HexRegex : " + TagManager.getHexPattern(false));
            Messages.DEBUG.sendConsole("FullRegex: " + TagManager.getSplitPattern(ConfigData.PREFIX_START_CHAR.get(), ConfigData.PREFIX_END_CHAR.get(), false));

            Messages.DEBUG.send(sender, "Brackets: " + (ConfigData.PREFIX_BRACKET_ENABLED.get() ? "Enabled" : "Disabled"));
            Messages.DEBUG.send(sender, "Hexadecimal: " + (!ConfigData.HEX_ENABLED.get() ? "Disabled" : (!ConfigData.HEX_NAMES.get() ? "Enabled" : "Names")));
            Messages.DEBUG.send(sender, "Format: '" + ConfigData.HEX_FORMAT.get() + "'");
            Messages.DEBUG.send(sender, "");

            Tag tag = TagManager.getTag(player, player, isPrefix);
            if (tag == null)
                return false;

            Messages.DEBUG.send(sender, "NewTag: '" + tag.getFullTag().replace('&', '%') + "' --");
            return true;
        }
        return false;
    }

    /**
     * Suffix only
     */
    private void onRemoveCmd() {
        if (isPrefix)
            return;

        if (!Permission.SUFFIX_REMOVE.hasPermission(sender)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length == 2) {
            player = getPlayer(player, args[1]);
            if (player == null)
                return; //Already handled
        }

        if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole("/suffix remove <name>");
            return;
        }

        Core.removeSuffix(player);
        if (player != sender)
            Messages.CMD_REMOVED_OTHER.send(sender, "suffix", getDisplayName(player));
    }

    /**
     * Suffix & Prefix
     */
    private void onDefaultCmd() {
        if (!(isPrefix ? Permission.PREFIX_CHANGE : Permission.SUFFIX_CHANGE).hasPermission(sender)) {
            Messages.NO_PERM_CMD.send(sender);
            return;
        }

        if (args.length >= 2) {
            player = getPlayer(sender, args[1]);
            if (player == null)
                return; //Already handled
        } else if (!isPlayer) {
            Messages.ERROR_TARGET_NEED.sendConsole();
            Messages.CMD_USAGE.sendConsole((isPrefix ? "/prefix <prefix>" : "/suffix <suffix>") + " <name>");
            return;
        }

        if (isPrefix) {
            if (Core.changePrefix(sender, player, args[0]) && player != sender)
                Messages.CMD_CHANGED_OTHER.send(sender, "prefix", getDisplayName(player));
        } else {
            if (Core.changeSuffix(sender, player, args[0]) && player != sender)
                Messages.CMD_CHANGED_OTHER.send(sender, "suffix", getDisplayName(player));
        }
    }

    /**
     * Get the targeted player
     *
     * @param sender Cmd sender
     * @param name   Player to target
     * @return @Nullable Player object of target
     */
    protected Object getPlayer(Object sender, String name) {
        if (sender != null && !Permission.TARGET_OTHER.hasPermission(sender)) {
            Messages.NO_PERMS_TARGET.send(sender);
            return null;
        }

        Object player = Statics.getPlayer(name);
        if (player == null) {
            Messages.ERROR_PLAYER.send(sender, name);
            return null;
        }
        if (player == sender) {
            Messages.ERROR_TARGET_SELF.send(sender);
            return null;
        }
        return player;
    }
}
