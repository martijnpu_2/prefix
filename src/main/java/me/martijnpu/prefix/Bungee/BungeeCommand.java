package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.Util.Interfaces.ICommand;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

class BungeeCommand extends ICommand {
    BungeeCommand() {
        Main.get().getProxy().getPluginManager().registerCommand(Main.get(), new BungeePrefix());
        Main.get().getProxy().getPluginManager().registerCommand(Main.get(), new BungeeTag());
        Main.get().getProxy().getPluginManager().registerCommand(Main.get(), new BungeeSuffix());
    }
}

class BungeePrefix extends Command implements TabExecutor {
    private final ICommand iCommand;

    BungeePrefix() {
        super("prefix", "");
        iCommand = new ICommand();
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        return iCommand.onPrefixTabComplete(commandSender, args);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        iCommand.onPrefixCommand(sender, (sender instanceof ProxiedPlayer), strings);
    }
}

class BungeeTag extends Command implements TabExecutor {
    private final ICommand iCommand;

    BungeeTag() {
        super("tag", "");
        iCommand = new ICommand();
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        return iCommand.onPrefixTabComplete(commandSender, args);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        iCommand.onPrefixCommand(sender, (sender instanceof ProxiedPlayer), strings);
    }
}

class BungeeSuffix extends Command implements TabExecutor {
    private final ICommand iCommand;

    BungeeSuffix() {
        super("suffix", "");
        iCommand = new ICommand();
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        return iCommand.onSuffixTabComplete(commandSender, args);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        iCommand.onSuffixCommand(sender, (sender instanceof ProxiedPlayer), strings);
    }
}
