package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.Util.Config.ConfigData;
import me.martijnpu.prefix.Util.Interfaces.IConfig;
import me.martijnpu.prefix.Util.Messages;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class BungeeFileManager implements IConfig {
    private static BungeeFileManager instance;
    private final File configFile = new File(Main.get().getDataFolder(), "config.yml");
    private Configuration configuration;
    private Configuration messagesConfiguration;
    private String locale = "en_US";

    private BungeeFileManager() {
        reload();
    }

    public static BungeeFileManager getInstance() {
        if (instance == null)
            instance = new BungeeFileManager();
        return instance;
    }

    public void printInitData() {
        Messages.PLUGIN.sendConsole("&aLoading " + Messages.values().length + " messages.");

        for (Messages message : Messages.values())
            if (!messagesConfiguration.contains(message.getPath()) && !message.getPath().equalsIgnoreCase("custom-does-not-exist"))
                Messages.WARN.sendConsole("&cMissing message &b\"" + message.getPath() + "\"&c Regenerate the file or copy the default from spigot");
    }

    //region Messages
    public String getMessage(String path) {
        if (messagesConfiguration == null)
            return path;
        return messagesConfiguration.getString(path, path);
    }

    private File messagesFile() {
        return new File(Main.get().getDataFolder(), "languages/" + locale + ".yml");
    }

    private void loadMessagesFile(boolean loadDefault) {
        locale = loadDefault ? "en_US" : ConfigData.LOCALE.get();
        if (!Main.get().getDataFolder().exists())
            Main.get().getDataFolder().mkdir();
        if (!messagesFile().getParentFile().exists())
            messagesFile().getParentFile().mkdir();

        if (!messagesFile().exists() && Main.get().getResourceAsStream("languages/" + locale + ".yml") == null) {
            Messages.WARN.sendConsole("Language '" + locale + "' does not exist. Switching back to English!");
            locale = "en_US";
        }

        if (!messagesFile().exists()) {
            try (InputStream in = Main.get().getResourceAsStream("languages/" + locale + ".yml")) {
                Files.copy(in, messagesFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!loadDefault)
            Messages.PLUGIN.sendConsole("Loading language pack: " + getMessage(Messages.LANGUAGE.getPath()));
    }

    private void reloadMessages(boolean loadDefault) {
        try {
            loadMessagesFile(loadDefault);
            messagesConfiguration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(messagesFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region Config
    private void saveDefaultConfig() {
        if (!Main.get().getDataFolder().exists())
            Main.get().getDataFolder().mkdir();

        if (!configFile.exists()) {
            try (InputStream in = Main.get().getResourceAsStream("config.yml")) {
                Files.copy(in, configFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void reloadConfig() {
        try {
            saveDefaultConfig();
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean getBoolean(String path, boolean def) {
        return configuration.getBoolean(path, def);
    }

    @Override
    public String getString(String path, String def) {
        return configuration.getString(path, def);
    }

    @Override
    public int getInt(String path, int def) {
        return configuration.getInt(path, def);
    }

    @Override
    public List<String> getStringList(String path, List<String> def) {
        List<String> list = configuration.getStringList(path);
        return list.isEmpty() ? def : list;
    }

    @Override
    public boolean isSet(String path) {
        return configuration.contains(path);
    }

    @Override
    public void set(String path, Object value) {
        configuration.set(path, value);
    }

    @Override
    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(Main.get().getDataFolder(), "config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reload() {
        reloadMessages(true);
        reloadConfig();
    }

    @Override
    public void loadLocale() {
        reloadMessages(false);
    }

    //endregion
}
