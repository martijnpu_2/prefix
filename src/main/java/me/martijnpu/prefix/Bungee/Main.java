package me.martijnpu.prefix.Bungee;

import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.Config.ConfigData;
import me.martijnpu.prefix.Util.Interfaces.IConfig;
import me.martijnpu.prefix.Util.Interfaces.IMessage;
import me.martijnpu.prefix.Util.Interfaces.IPrefix;
import me.martijnpu.prefix.Util.Interfaces.IStatic;
import me.martijnpu.prefix.Util.Messages;
import me.martijnpu.prefix.Util.PrefixAdapter;
import me.martijnpu.prefix.Util.Statics;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import org.bstats.bungeecord.Metrics;

import java.util.concurrent.TimeUnit;

public class Main extends Plugin implements IPrefix {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Statics.isProxy = true;
        PrefixAdapter.setAdapter(this);

        try {
            if (Integer.parseInt("%%__RESOURCE__%%") == 70359)
                Messages.PLUGIN.sendConsole("&aThanks for downloading");
        } catch (NumberFormatException ignore) {
        }

        if (LuckPermsConnector.checkLuckPermsAbsent()) {
            Messages.WARN.sendConsole("Couldn't found any valid LuckPerm v5 instance");
            Messages.WARN.sendConsole("Disabling plugin...");
            this.onDisable();
            return;
        }
        ProxyServer.getInstance().getScheduler().schedule(this, this::checkForUpdates, 0, 1, TimeUnit.DAYS);

        BungeeFileManager.getInstance().printInitData();
        ConfigData.getInstance().printInitData();
        new BungeeCommand();
        new BungeePlayerJoin();

        if (ConfigData.BSTATS.get())
            new Metrics(this, 10921);

        Messages.PLUGIN.sendConsole("&aWe're up and running");
    }

    @Override
    public void onDisable() {
        Messages.PLUGIN.sendConsole("&aDisabled successful");
    }

    private void checkForUpdates() {
        ProxyServer.getInstance().getScheduler().runAsync(this, () -> Statics.checkForUpdate(Double.parseDouble(getDescription().getVersion())));
    }

    @Override
    public IMessage getMessagesAdapter() {
        return BungeeMessages.getInstance();
    }

    @Override
    public IConfig getConfigAdapter() {
        return BungeeFileManager.getInstance();
    }

    @Override
    public IStatic getStaticsAdapter() {
        return BungeeStatics.getInstance();
    }
}
