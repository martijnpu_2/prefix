package me.martijnpu.prefix;

import me.martijnpu.prefix.Util.Color;
import me.martijnpu.prefix.Util.Config.ConfigData;
import me.martijnpu.prefix.Util.HexColor;
import me.martijnpu.prefix.Util.Permission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static me.martijnpu.prefix.Util.Statics.debug;
import static me.martijnpu.prefix.Util.Statics.getOnlinePlayers;

public class TabComplete {
    public List<String> onSuffixTabComplete(Object sender, String[] args) {
        List<String> list = new ArrayList<>();

        if (args.length == 1) {
            list.add("list");
            list.add("help");
            if (Permission.SUFFIX_CHANGE.hasPermission(sender))
                list.add("reset");
            if (Core.hasAnColorPermission(sender, Permission.COLOR_SUFFIX))
                list.add("color");
            if (Permission.ADMIN.hasPermission(sender) && debug)
                list.add("debug");
            if (Permission.SUFFIX_REMOVE.hasPermission(sender))
                list.add("remove");
            return sort(list, args);
        }

        switch (args[0].toLowerCase()) {
            case "color":
                if (!Core.hasAnColorPermission(sender, Permission.COLOR_SUFFIX))
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_SUFFIX));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "reset":
                if (!Permission.SUFFIX_CHANGE.hasPermission(sender))
                    break;
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "remove":
                if (!Permission.SUFFIX_REMOVE.hasPermission(sender))
                    break;
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;
        }
        return sort(list, args);
    }

    public List<String> onPrefixTabComplete(Object sender, String[] args) {
        List<String> list = new ArrayList<>();

        if (args.length == 1) {
            list.add("list");
            list.add("help");
            if (Permission.PREFIX_CHANGE.hasPermission(sender))
                list.add("reset");
            if (Core.hasAnColorPermission(sender, Permission.COLOR_PREFIX))
                list.add("color");
            if (Core.hasAnColorPermission(sender, Permission.COLOR_NAME))
                list.add("name");
            if (ConfigData.PREFIX_BRACKET_ENABLED.get() && ConfigData.PREFIX_BRACKET_COLOR.get().isEmpty() && Core.hasAnColorPermission(sender, Permission.COLOR_BRACKET))
                list.add("bracket");
            if (Permission.ADMIN.hasPermission(sender)) {
                list.add("reload");
                list.add("version");
                if (debug)
                    list.add("debug");
            }
            return sort(list, args);
        }

        switch (args[0].toLowerCase()) {
            case "color":
                if (!Core.hasAnColorPermission(sender, Permission.COLOR_PREFIX))
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_PREFIX));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "name":
                if (!Core.hasAnColorPermission(sender, Permission.COLOR_NAME))
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_NAME));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "bracket":
                if (!ConfigData.PREFIX_BRACKET_ENABLED.get() || !ConfigData.PREFIX_BRACKET_ENABLED.get() || !ConfigData.PREFIX_BRACKET_COLOR.get().isEmpty())
                    break;
                if (args.length == 2)
                    list.addAll(getColorPerms(sender, Permission.COLOR_BRACKET));
                if (args.length == 3 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;

            case "reset":
                if (!Permission.PREFIX_CHANGE.hasPermission(sender))
                    break;
                if (args.length == 2 && Permission.TARGET_OTHER.hasPermission(sender))
                    list.addAll(getOnlinePlayers());
                break;
        }
        return sort(list, args);
    }

    private List<String> sort(List<String> list, String[] args) {
        if (args.length == 5)
            list.add("Yeeeeeeet"); //EasterEgg
        else if (args.length == 10)
            list.add(">;D"); //EasterEgg

        list.removeIf((x) -> !x.toLowerCase().startsWith(args[args.length - 1].toLowerCase()));
        Collections.sort(list);
        return list;
    }

    private List<String> getColorPerms(Object sender, Permission permission) {
        try {
            List<String> result = new ArrayList<>();
            result.add("reset");
            for (Color c : Color.values())
                if (c != Color.HEXADECIMAL && Core.hasColorPermission(sender, c, permission))
                    result.add(c.getName());

            if (ConfigData.HEX_ENABLED.get()) {
                result.add(ConfigData.HEX_FORMAT.get());
                if (ConfigData.HEX_NAMES.get())
                    for (HexColor hex : HexColor.values())
                        result.add(hex.getName());
            }
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }
}
