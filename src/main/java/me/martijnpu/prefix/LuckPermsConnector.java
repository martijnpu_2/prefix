package me.martijnpu.prefix;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.types.PrefixNode;
import net.luckperms.api.node.types.SuffixNode;
import net.luckperms.api.query.QueryOptions;

import java.util.UUID;

public class LuckPermsConnector {
    private static final int nodeWeight = 2147483647;
    private static LuckPermsConnector instance;
    private LuckPerms api;

    private LuckPermsConnector() {
        try {
            api = LuckPermsProvider.get();
        } catch (IllegalStateException ex) {
            api = null;
        }
    }

    private static LuckPermsConnector getInstance() {
        if (instance == null)
            instance = new LuckPermsConnector();
        return instance;
    }

    public static boolean checkLuckPermsAbsent() {
        return getInstance().api == null;
    }

    public static String getPrefix(UUID uuid) {
        return loadUser(uuid).getCachedData().getMetaData(QueryOptions.defaultContextualOptions()).getPrefix();
    }

    public static String getSuffix(UUID uuid) {
        return loadUser(uuid).getCachedData().getMetaData(QueryOptions.defaultContextualOptions()).getSuffix();
    }

    static void setPrefix(UUID uuid, String prefix) {
        PrefixNode prefixNode = PrefixNode.builder(prefix, nodeWeight).build();
        User user = loadUser(uuid);
        resetPrefix(uuid);
        user.data().add(prefixNode);
        saveUser(user);
    }

    static void setSuffix(UUID uuid, String suffix) {
        SuffixNode suffixNode = SuffixNode.builder(suffix, nodeWeight).build();
        User user = loadUser(uuid);
        resetSuffix(uuid);
        user.data().add(suffixNode);
        saveUser(user);
    }

    static void resetPrefix(UUID uuid) {
        try {
            User user = loadUser(uuid);
            PrefixNode prefixNode = PrefixNode.builder(getPrefix(uuid), nodeWeight).build();
            user.data().clear(n -> n.getType().matches(prefixNode));
            saveUser(user);
        } catch (NullPointerException ignored) {
        }
    }

    static void resetSuffix(UUID uuid) {
        try {
            User user = loadUser(uuid);
            SuffixNode suffixNode = SuffixNode.builder(getSuffix(uuid), nodeWeight).build();
            user.data().clear(n -> n.getType().matches(suffixNode));
            saveUser(user);
        } catch (NullPointerException ignored) {
        }
    }

    private static User loadUser(UUID uuid) {
        return getInstance().api.getUserManager().getUser(uuid);
    }

    private static void saveUser(User user) {
        getInstance().api.getUserManager().saveUser(user).thenRun(() -> getInstance().api.getMessagingService().ifPresent(service -> service.pushUserUpdate(user)));
    }
}
