package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.Config.ConfigData;
import me.martijnpu.prefix.Util.Interfaces.IConfig;
import me.martijnpu.prefix.Util.Interfaces.IMessage;
import me.martijnpu.prefix.Util.Interfaces.IPrefix;
import me.martijnpu.prefix.Util.Interfaces.IStatic;
import me.martijnpu.prefix.Util.Messages;
import me.martijnpu.prefix.Util.PrefixAdapter;
import me.martijnpu.prefix.Util.Statics;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements IPrefix {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Statics.isProxy = false;
        PrefixAdapter.setAdapter(this);

        try {
            if (Integer.parseInt("%%__RESOURCE__%%") == 70359)
                Messages.PLUGIN.sendConsole("&aThanks for downloading");
        } catch (NumberFormatException ignore) {
        }

        if (LuckPermsConnector.checkLuckPermsAbsent()) {
            Messages.WARN.sendConsole("Couldn't found any valid LuckPerm v5 instance");
            Messages.WARN.sendConsole("Disabling plugin...");
            this.setEnabled(false);
            return;
        }
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::checkForUpdates, 0, 1728000);

        BukkitFileManager.getInstance().printInitData();
        ConfigData.getInstance().printInitData();
        new BukkitPrefixCommand();
        new BukkitSuffixCommand();
        new BukkitPlayerJoin();

        if (ConfigData.BSTATS.get())
            new Metrics(this, 10920);

        Messages.PLUGIN.sendConsole("&aWe're up and running");
    }

    @Override
    public void onDisable() {
        Messages.PLUGIN.sendConsole("&aDisabled successful");
    }

    private void checkForUpdates() {
        Bukkit.getScheduler().runTaskAsynchronously(this, () -> Statics.checkForUpdate(Double.parseDouble(getDescription().getVersion())));
    }

    @Override
    public IMessage getMessagesAdapter() {
        return BukkitMessages.getInstance();
    }

    @Override
    public IConfig getConfigAdapter() {
        return BukkitFileManager.getInstance();
    }

    @Override
    public IStatic getStaticsAdapter() {
        return BukkitStatics.getInstance();
    }
}
