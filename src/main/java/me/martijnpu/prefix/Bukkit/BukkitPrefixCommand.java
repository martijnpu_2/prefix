package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.Util.Interfaces.ICommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Objects;

class BukkitPrefixCommand extends ICommand implements CommandExecutor, TabCompleter {
    BukkitPrefixCommand() {
        Objects.requireNonNull(Main.get().getCommand("prefix")).setExecutor(this);
        Objects.requireNonNull(Main.get().getCommand("prefix")).setTabCompleter(this);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        return onPrefixTabComplete(sender, args);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return onPrefixCommand(sender, (sender instanceof Player), args);
    }
}

class BukkitSuffixCommand extends ICommand implements CommandExecutor, TabCompleter {
    BukkitSuffixCommand() {
        Objects.requireNonNull(Main.get().getCommand("suffix")).setExecutor(this);
        Objects.requireNonNull(Main.get().getCommand("suffix")).setTabCompleter(this);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        return onSuffixTabComplete(sender, args);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return onSuffixCommand(sender, (sender instanceof Player), args);
    }
}
