package me.martijnpu.prefix.Util;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.UUID;

public class Statics extends PrefixAdapter {
    public static boolean debug;
    public static boolean isProxy;
    public static double newVersion;
    public static double currVersion;

    public static void onPlayerJoin(Object player) {
        if ((newVersion > currVersion) && Permission.ADMIN.hasPermission(player)) {
            String text = "&eNew version of Prefix available!"
                    + "\nCurrent version: &f" + currVersion
                    + "\n&eNew version: &f" + newVersion;
            Messages.PLUGIN.sendBig(player, text);
        }
    }

    public static void checkForUpdate(double pluginVersion) {
        currVersion = pluginVersion;
        try {
            getSpigotVersion();
            if (newVersion > pluginVersion) {
                Messages.PLUGIN.sendBig(null, "" +
                        "&b------------------------------------------------\n" +
                        "&cA new version of Prefix is available.\n" +
                        "&bCurrent version: &a" + pluginVersion + "&b, new version: &a" + newVersion + "&b.\n" +
                        "&bURL: &ahttps://www.spigotmc.org/resources/prefix.70359/\n" +
                        "&b------------------------------------------------");
            } else if (newVersion < pluginVersion) {
                Messages.PLUGIN.sendConsole("&bYour version of PrefiX (v" + currVersion + ") is ahead of the official release!");
            } else {
                Messages.PLUGIN.sendConsole("&aYour version of PrefiX (v" + currVersion + ") is up to date!");
            }
        } catch (Exception e) {
            Messages.PLUGIN.sendBig(null, "" +
                    "&b------------------------------------------------\n" +
                    "&cUnable to receive version from Spigot.\n" +
                    "&cCurrent version: " + pluginVersion + ".\n" +
                    "&cURL: &ahttps://www.spigotmc.org/resources/prefix.70359/\n" +
                    "&b------------------------------------------------");
            newVersion = 0;
        }
    }

    public static String getServerVersion() {
        return getStaticsAdapter().getServerVersion();
    }

    public static List<String> getOnlinePlayers() {
        return getStaticsAdapter().getOnlinePlayers();
    }

    public static String getDisplayName(Object player) {
        return getStaticsAdapter().getDisplayName(player);
    }

    public static UUID getUUID(Object sender) {
        return getStaticsAdapter().getUUID(sender);
    }

    public static Object getPlayer(String name) {
        return getStaticsAdapter().getPlayer(name);
    }

    private static void getSpigotVersion() throws IOException {
        final HttpsURLConnection connection = (HttpsURLConnection) new URL("https://api.spigotmc.org/legacy/update.php?resource=70359").openConnection();
        connection.setRequestMethod("GET");
        newVersion = Double.parseDouble(new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine());
    }
}
