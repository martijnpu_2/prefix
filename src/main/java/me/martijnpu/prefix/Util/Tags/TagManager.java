package me.martijnpu.prefix.Util.Tags;

import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.Config.ConfigData;
import me.martijnpu.prefix.Util.HexColor;
import me.martijnpu.prefix.Util.Messages;
import me.martijnpu.prefix.Util.Permission;
import net.md_5.bungee.api.ChatColor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static me.martijnpu.prefix.Util.Statics.getUUID;

@SuppressWarnings("RegExpUnnecessaryNonCapturingGroup")
public class TagManager {

    public final static String hexReplacement = "rrggbb";
    private final static String colorCodePattern = "(?:&[" + ChatColor.ALL_CODES + "])";

    /**
     * Get regex for capturing formatted hex pattern
     * Only use for Patterns
     *
     * @param withNames Include hexadecimal names
     * @return Non-capturable regex group
     */
    public static String getHexPattern(boolean withNames) {
        String basic = ConfigData.HEX_FORMAT.get();
        basic = basic.replaceAll("([\\\\+*?\\[\\](){}|.^$])", "\\\\$1");
        basic = basic.replace(hexReplacement, getAllHexColors(withNames) + "[<>]?");
        return "(?:" + basic + ")";
    }

    /**
     * Get regex for capturing default hex pattern
     * Only use for Patterns
     *
     * @param withNames Include hexadecimal names
     * @return Non-capturable regex group
     */
    private static String getAllHexColors(boolean withNames) {
        StringBuilder sb = new StringBuilder("[0-9a-fA-F]{6}");
        if (withNames)
            for (HexColor value : HexColor.values())
                sb.append("|").append(value.getName());
        return "(?:" + sb + ")";
    }

    /**
     * Checks whether the input is hexadecimal (Number/Name)
     */
    public static boolean isHexadecimal(String input) {
        return Pattern.compile(getHexPattern(true), Pattern.CASE_INSENSITIVE).matcher(input).matches();
    }

    /**
     * Available groups: StartColor, StartChar, NameColor, Tag, EndChar, EndColor
     */
    public static Pattern getSplitPattern(String startChar, String endChar, boolean withNames) {
        String colorPattern = "(?:" + colorCodePattern + "|" + getHexPattern(withNames) + ")*+";
        String stringPattern = "^.*?(?>(?<StartColor>" + colorPattern + ")(?<StartChar>" + Pattern.quote(startChar) + ")(?<NameColor>" + colorPattern + "))(?<Tag>.*?)(?<EndChar>" + Pattern.quote(endChar) + ")(?<EndColor>" + colorPattern + ")$";
        return Pattern.compile(stringPattern, Pattern.CASE_INSENSITIVE);
    }

    /**
     * Available groups: TagBefore, Tag, TagAfter
     */
    static Pattern getTagSplitPattern() {
        String pattern = "(?<tagBefore>" + getHexPattern(true) + "?+)(?<Tag>.*?)(?<TagAfter>" + getHexPattern(true) + "??)";
        return Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
    }

    /**
     * Available groups: Color, Format
     */
    static Pattern getColorSplitPattern() {
        String colorPattern = "(?:(?:&[0123456789abcdef])|" + getHexPattern(true) + ")*+";
        String formatPattern = "(?:&[klmnor])*";
        String pattern = "(?<Color>" + colorPattern + ")?(?<Format>" + formatPattern + ")?$";
        return Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
    }

    /**
     * Get Tag of player
     * Including all pre-checks & validity
     */
    public static Tag getTag(Object player, Object sender, boolean isPrefix) {
        String startChar = isPrefix ? ConfigData.PREFIX_START_CHAR.get() : ConfigData.SUFFIX_START_CHAR.get();
        String endChar = isPrefix ? ConfigData.PREFIX_END_CHAR.get() : ConfigData.SUFFIX_END_CHAR.get();
        String tag = isPrefix ? LuckPermsConnector.getPrefix(getUUID(player)) : LuckPermsConnector.getSuffix(getUUID(player));

        // Pre-checks
        if (startChar == null) {
            Messages.WARN.send(sender, "Couldn't find a valid Start-char in the config. Please contact your administrator");
            return null;
        }
        if (endChar == null) {
            Messages.WARN.send(sender, "Couldn't find a valid End-char in the config. Please contact your administrator");
            return null;
        }

        if (tag == null) {
            Messages.WARN.send(sender, "&3Creating tag from scratch...");
            Messages.WARN.sendConsole("Didn't receive an tag from LuckPerms! Possible causes are an invalid tag or none tag available.");
            tag = "";

        } else if (!tag.contains(startChar) || !tag.contains(endChar)) {
            Messages.WARN.send(sender, "&4Couldn't find a valid tag. Please contact your administrator.");
            Messages.WARN.send(sender, "&3Trying to construct one...");
            Messages.WARN.sendConsole("Couldn't find a valid tag!"
                    + "\nFound \"" + tag.replace('&', '#') + "\" but was unable to find Start-Char or End-Char.\n"
                    + "Use the following format: &c\"" + startChar + "tag" + endChar + "\".");
        }

        Messages.DEBUG.sendConsole("OldTag: '" + tag.replaceAll("&", "#") + "' --");

        if (!startChar.isEmpty() && tag.indexOf(startChar, tag.indexOf(startChar) + startChar.length()) != -1) {
            Messages.WARN.send(sender, "&4Found multiple start-characters in your tag. (&f" + startChar + "&4). Please contact your administrator");
            Messages.WARN.send(sender, "Trying to construct one...");
        }

        if (!endChar.isEmpty() && tag.indexOf(endChar, tag.indexOf(endChar) + endChar.length()) != -1) {
            Messages.WARN.send(sender, "&4Found multiple end-characters in your tag. (&f" + endChar + "&4). Please contact your administrator");
            Messages.WARN.send(sender, "Trying to construct one...");
        }

        return new Tag(tag, startChar, endChar);
    }

    /**
     * Checks colorcode perms and max length.
     * <i>Error messages are already handled</i>
     *
     * @return whether the checks are all valid.
     */
    public static boolean isInvalidTag(Object sender, String tag, Permission charPerm, int maxLength) {
        Pattern pattern = Pattern.compile(colorCodePattern + "++", Pattern.CASE_INSENSITIVE);
        if (pattern.matcher(tag).find()) {
            if (charPerm.hasPermission(sender)) {
                int foundCodes = 0;
                Matcher basicMatcher = pattern.matcher(tag);
                while (basicMatcher.find()) {
                    foundCodes++;
                    maxLength += basicMatcher.group().length();
                }
                Messages.DEBUG.sendConsole("Found " + foundCodes + " colorcode");
            } else {
                Messages.ERROR_COLOR.send(sender);
                return true;
            }
        }

        Pattern hexPattern = Pattern.compile(getHexPattern(true) + "++", Pattern.CASE_INSENSITIVE);
        if (hexPattern.matcher(tag).find()) {
            if (!ConfigData.HEX_ENABLED.get()) {
                Messages.ERROR_HEX.send(sender);
                return true;
            }

            if (charPerm.hasPermission(sender)) {
                int foundCodes = 0;
                Matcher hexMatcher = hexPattern.matcher(tag);
                while (hexMatcher.find()) {
                    if (!ConfigData.HEX_GRADIENT.get() && (hexMatcher.group().contains("<") || hexMatcher.group().contains(">"))) {
                        Messages.ERROR_HEX.send(sender);
                        return true;
                    }
                    foundCodes++;
                    maxLength += hexMatcher.group().length();
                }
                Messages.DEBUG.sendConsole("Found " + foundCodes + " hexcodes");
            } else {
                Messages.ERROR_COLOR.send(sender);
                return true;
            }
        }

        if (tag.length() > maxLength) {
            if (Permission.PREFIX_CHAR.hasPermission(sender))
                Messages.ERROR_COLOR_LENGTH.send(sender, ConfigData.PREFIX_MAX_LENGTH.get().toString());
            else
                Messages.ERROR_LENGTH.send(sender, ConfigData.PREFIX_MAX_LENGTH.get().toString());
            return true;
        }

        Messages.DEBUG.sendConsole("Length fine");
        return false;
    }
}
