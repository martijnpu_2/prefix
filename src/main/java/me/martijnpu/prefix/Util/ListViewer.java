package me.martijnpu.prefix.Util;

import me.martijnpu.prefix.Util.Config.ConfigData;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import static me.martijnpu.prefix.Core.hasColorPermission;

public class ListViewer {
    public static void showPrefixColorList(Object sender) {
        boolean reduceSpaces = sender == null;
        TextComponent colorList = getTitle("Prefix   |   Name", reduceSpaces);
        TextComponent separator = new TextComponent("  |  ");
        separator.setColor(ChatColor.GRAY);

        for (Color color : Color.values()) {
            if (color == Color.HEXADECIMAL)
                continue;

            TextComponent nameColor = checkColor(sender, color, "name");
            TextComponent prefixColor = checkColor(sender, color, "prefix");

            TextComponent colorLine = new TextComponent();
            colorLine.addExtra(prefixColor);
            colorLine.addExtra(separator);
            colorLine.addExtra(nameColor);
            colorList.addExtra("\n" + DefaultFontInfo.adaptSpaces(ChatColor.stripColor(colorLine.toPlainText()), false, false, reduceSpaces));
            colorList.addExtra(colorLine);
        }
        if (Permission.PREFIX_CHAR.hasPermission(sender))
            colorList.addExtra(getCharLine(reduceSpaces));

        Messages.LIST.sendBig(sender, colorList);
    }

    public static void showSuffixColorList(Object sender) {
        boolean reduceSpaces = sender == null;
        TextComponent colorList = getTitle("Suffix", reduceSpaces);

        for (Color color : Color.values()) {
            if (color == Color.HEXADECIMAL)
                continue;

            TextComponent suffixColor = checkColor(sender, color, "suffix");

            colorList.addExtra("\n" + DefaultFontInfo.adaptSpaces(ChatColor.stripColor(suffixColor.toPlainText()), false, false, reduceSpaces));
            colorList.addExtra(suffixColor);
        }
        if (Permission.PREFIX_CHAR.hasPermission(sender))
            colorList.addExtra(getCharLine(reduceSpaces));

        Messages.LIST.sendBig(sender, colorList);
    }

    private static TextComponent getTitle(String text, boolean reduceSpaces) {
        text = "&7&l" + text;
        return new TextComponent(Messages.LIST.convert(DefaultFontInfo.adaptSpaces(text, true, true, reduceSpaces)));
    }

    private static TextComponent checkColor(Object sender, Color color, String type) {
        TextComponent nameColor = new TextComponent(Messages.LIST.convert(color.getColorString("")));
        String cmd = "/" + type + " color " + color.getName();
        if (type.equals("name"))
            cmd = "/prefix name " + color.getName();

        if (sender == null || hasColorPermission(sender, color, Permission.COLOR_NAME)) {
            nameColor.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd));
            nameColor.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST_APPLY.convert(type)));
        } else {
            nameColor.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST_NO_PERM.convert(type)));
            nameColor.setStrikethrough(true);
        }
        return nameColor;
    }

    private static TextComponent getCharLine(boolean reduceSpaces) {
        TextComponent color = new TextComponent("\n" + DefaultFontInfo.adaptSpaces("&&", true, false, reduceSpaces));
        color.setColor(ChatColor.GOLD);
        color.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST.convert("&fYou can use colorcodes")));

        if (ConfigData.HEX_ENABLED.get()) {
            TextComponent hex = new TextComponent("\n" + DefaultFontInfo.adaptSpaces(ConfigData.HEX_FORMAT.get(), true, false, reduceSpaces));
            hex.setColor(ChatColor.GOLD);
            hex.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Messages.LIST.convert("&fYou can use hexadecimal")));
            color.addExtra(hex);
        }
        return color;
    }
}