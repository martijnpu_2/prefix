package me.martijnpu.prefix.Util;

public enum Permission {
    TARGET_OTHER("prefix.other"),
    ADMIN("prefix.admin"),
    PREFIX_CHAR("prefix.char"),
    SUFFIX_CHAR("suffix.char"),
    PREFIX_CHANGE("prefix.change"),
    SUFFIX_CHANGE("suffix.change"),
    SUFFIX_REMOVE("suffix.remove"),
    BLACKLIST_BYPASS("prefix.blacklist"),
    COLOR_BASE("prefix.color"),
    COLOR_PREFIX("prefix.color.prefix"),
    COLOR_NAME("prefix.color.name"),
    COLOR_SUFFIX("suffix.color"),
    COLOR_BRACKET("prefix.color.bracket");

    private final String path;

    Permission(String path) {
        this.path = path;
    }

    public static boolean hasPermission(Object sender, String path) {
        if (sender == null)
            return true;

        if (Statics.isProxy)
            return ((net.md_5.bungee.api.CommandSender) sender).hasPermission(path);
        else
            return ((org.bukkit.command.CommandSender) sender).hasPermission(path);
    }

    public boolean hasPermission(Object sender) {
        if (sender == null)
            return true;
        return hasPermission(sender, path);
    }

    public String getPath() {
        return path;
    }
}
