package me.martijnpu.prefix.Util.Interfaces;

import java.util.List;

public interface IConfig {
    boolean getBoolean(String path, boolean def);

    String getString(String path, String def);

    int getInt(String path, int def);

    List<String> getStringList(String path, List<String> def);

    boolean isSet(String path);

    void set(String path, Object value);

    void saveConfig();

    void reload();

    void loadLocale();
}
