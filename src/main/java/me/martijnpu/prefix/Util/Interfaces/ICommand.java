package me.martijnpu.prefix.Util.Interfaces;

import me.martijnpu.prefix.CmdHandler;
import me.martijnpu.prefix.TabComplete;

import java.util.List;

public class ICommand {
    private final CmdHandler cmdHandler;
    private final TabComplete tabComplete;

    public ICommand() {
        cmdHandler = new CmdHandler();
        tabComplete = new TabComplete();
    }

    public boolean onPrefixCommand(Object sender, boolean isPlayer, String[] args) {
        cmdHandler.onPrefixCommand(sender, isPlayer, args);
        return true;
    }

    public boolean onSuffixCommand(Object sender, boolean isPlayer, String[] args) {
        cmdHandler.onSuffixCommand(sender, isPlayer, args);
        return true;
    }

    public List<String> onPrefixTabComplete(Object sender, String[] args) {
        return tabComplete.onPrefixTabComplete(sender, args);
    }

    public List<String> onSuffixTabComplete(Object sender, String[] args) {
        return tabComplete.onSuffixTabComplete(sender, args);
    }
}
