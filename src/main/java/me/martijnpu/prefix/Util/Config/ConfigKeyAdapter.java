package me.martijnpu.prefix.Util.Config;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

import static me.martijnpu.prefix.Util.PrefixAdapter.getConfigAdapter;

interface ConfigKeyAdapter {
    BiFunction<String, Boolean, Boolean> GET_BOOLEAN = getConfigAdapter()::getBoolean;
    BiFunction<String, String, String> GET_STRING = getConfigAdapter()::getString;
    BiFunction<String, List<String>, List<String>> GET_STRINGLIST = getConfigAdapter()::getStringList;
    BiFunction<String, Integer, Integer> GET_INTEGER = getConfigAdapter()::getInt;

    static <T> ConfigKey<T> customKey(String path, T def, BiFunction<String, T, T> function) {
        return new ConfigKey<>(function, path, def);
    }

    static ConfigKey<Boolean> booleanKey(String path, boolean def) {
        return customKey(path, def, GET_BOOLEAN);
    }

    static ConfigKey<Integer> integerKey(String path, int def) {
        return customKey(path, def, GET_INTEGER);
    }

    static ConfigKey<String> stringKey(String path, String def) {
        return customKey(path, def, GET_STRING);
    }

    static ConfigKey<List<String>> stringListKey(String path, String[] def) {
        List<String> list = Arrays.asList(def);
        return customKey(path, list, GET_STRINGLIST);
    }
}