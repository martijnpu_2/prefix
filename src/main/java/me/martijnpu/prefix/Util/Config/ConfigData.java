package me.martijnpu.prefix.Util.Config;

import me.martijnpu.prefix.Util.Messages;
import me.martijnpu.prefix.Util.Statics;
import me.martijnpu.prefix.Util.Tags.TagManager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static me.martijnpu.prefix.Util.Config.ConfigKey.hide;
import static me.martijnpu.prefix.Util.Config.ConfigKeyAdapter.*;
import static me.martijnpu.prefix.Util.PrefixAdapter.getConfigAdapter;

public class ConfigData {
    public static final ConfigKey<Boolean> DEBUG = hide(booleanKey("debug", false));
    public static final ConfigKey<Boolean> BSTATS = hide(booleanKey("bstats", true));
    public static final ConfigKey<Integer> PREFIX_MAX_LENGTH = integerKey("prefix.max-length", 16);
    public static final ConfigKey<String> PREFIX_START_CHAR = stringKey("prefix.start-character", "[");
    public static final ConfigKey<String> PREFIX_END_CHAR = stringKey("prefix.end-character", "] ");
    public static final ConfigKey<Boolean> PREFIX_BRACKET_ENABLED = booleanKey("prefix.bracket.enable", false);
    public static final ConfigKey<String> PREFIX_BRACKET_COLOR = stringKey("prefix.bracket.change-color", "");
    public static final ConfigKey<Integer> SUFFIX_MAX_LENGTH = integerKey("suffix.max-length", 8);
    public static final ConfigKey<String> SUFFIX_START_CHAR = stringKey("suffix.start-character", " <");
    public static final ConfigKey<String> SUFFIX_END_CHAR = stringKey("suffix.end-character", ">");
    public static final ConfigKey<Boolean> HEX_ENABLED = booleanKey("general.hexadecimal.enabled", false);
    public static final ConfigKey<String> HEX_FORMAT = stringKey("general.hexadecimal.format", "{#rrggbb}");
    public static final ConfigKey<Boolean> HEX_GRADIENT = booleanKey("general.hexadecimal.gradient", false);
    public static final ConfigKey<Boolean> HEX_NAMES = booleanKey("general.hexadecimal.hex-names", false);
    public static final ConfigKey<Boolean> HIDE_WARNING = booleanKey("general.hide-warnings", false);
    public static final ConfigKey<List<String>> GENERAL_WHITELIST = stringListKey("general.whitelist", new String[]{"\\w", "\\p{Punct}"});
    public static final ConfigKey<List<String>> GENERAL_BLACKLIST = stringListKey("general.blacklist", new String[]{"fuck", "admin", "owner"});
    public static final ConfigKey<String> LOCALE = stringKey("general.locale", "en_US");

    private static List<ConfigKey<?>> KEYS;
    private static ConfigData instance;

    private ConfigData() {
        KEYS = new ArrayList<>();

        try {
            for (Field declaredField : this.getClass().getDeclaredFields())
                if (declaredField.getType().equals(ConfigKey.class))
                    KEYS.add((ConfigKey<?>) declaredField.get(this));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        reload(null);
    }

    public static ConfigData getInstance() {
        if (instance == null)
            instance = new ConfigData();
        return instance;
    }

    public void printInitData() {
        Messages.PLUGIN.sendConsole("&aLoaded " + KEYS.size() + " config values");
    }

    public void reload(Object sender) {
        getConfigAdapter().reload();

        boolean save = false;
        for (ConfigKey<?> key : KEYS)
            if (key.setDefault(false))
                save = true;

        if (checkForValidData())
            save = true;

        if (save)
            getConfigAdapter().saveConfig();
        checkForDebug();

        getConfigAdapter().loadLocale();

        if (save)
            Messages.ERROR_COMMAND.send(sender);
        else if (instance != null) //First time
            Messages.CMD_RELOAD.send(sender);
    }

    private void checkForDebug() {
        Statics.debug = ConfigData.DEBUG.get();
        Messages.DEBUG.sendConsole("&4Debugmodus enabled. Be aware for spam!");
    }

    /**
     * Check config for invalid fields
     *
     * @return Config needs to be saved
     */
    private boolean checkForValidData() {
        if (!HEX_FORMAT.get().contains("rrggbb")) {
            Messages.WARN.sendConsole("&4Config value \"Hexadecimal format\" doesn't contain required \"" + TagManager.hexReplacement + "\". Resetting to default value...");
            HEX_FORMAT.setDefault(true);
            return true;
        }
        return false;
    }
}
