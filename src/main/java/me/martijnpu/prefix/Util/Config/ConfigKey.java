package me.martijnpu.prefix.Util.Config;

import me.martijnpu.prefix.Util.Messages;

import java.util.function.BiFunction;

import static me.martijnpu.prefix.Util.PrefixAdapter.getConfigAdapter;

public class ConfigKey<T> {
    private final BiFunction<String, T, T> function;
    private final String path;
    private final T def;
    private boolean hidden = false;

    ConfigKey(BiFunction<String, T, T> function, String path, T def) {
        this.function = function;
        this.path = path;
        this.def = def;
    }

    static <T> ConfigKey<T> hide(ConfigKey<T> key) {
        key.hidden = true;
        return key;
    }

    private boolean isSet() {
        return getConfigAdapter().isSet(path);
    }

    boolean setDefault(boolean force) {
        if (!force) {
            if (hidden || isSet())
                return false;
            Messages.WARN.sendConsole("&4Adding missing key " + path + " as '" + def + "'");
        }
        getConfigAdapter().set(path, def);
        return true;
    }

    public T get() {
        return function.apply(path, def);
    }
}