package me.martijnpu.prefix.Util;

import me.martijnpu.prefix.Util.Interfaces.IConfig;
import me.martijnpu.prefix.Util.Interfaces.IMessage;
import me.martijnpu.prefix.Util.Interfaces.IPrefix;
import me.martijnpu.prefix.Util.Interfaces.IStatic;

public class PrefixAdapter {
    private static IPrefix adapter;

    public static void setAdapter(IPrefix adapter) {
        PrefixAdapter.adapter = adapter;
    }

    static IMessage getMessagesAdapter() {
        return adapter.getMessagesAdapter();
    }

    public static IConfig getConfigAdapter() {
        return adapter.getConfigAdapter();
    }

    static IStatic getStaticsAdapter() {
        return adapter.getStaticsAdapter();
    }
}
