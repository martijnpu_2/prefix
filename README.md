## Spigot
See the spigot page for downloads:
[https://www.spigotmc.org/resources/prefix.70359/](https://www.spigotmc.org/resources/prefix.70359/)

## Looking for support?
Don't feel ashamed to join [our Discord](https://discord.com/invite/2RHYvNy) server and ask your question.